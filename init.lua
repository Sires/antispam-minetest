-- Configuration here
local message_size_limit = 140
local admin = ""
local blacklist = {["!"] = 3, ["?"] = 5, ["A"] = 8, ["H"] = 6}
-- Ends here

local storage = minetest.get_mod_storage()

local function countSubstring(s1, s2)
    return select(2, s1:gsub(s2, ""))
end

minetest.register_on_chat_message(function(name, message)
	local spam = false
	if #message > message_size_limit then
		spam = true
		minetest.chat_send_player(name, "Please don't spam! If you didn't spam try making smaller messages")
	end
	if string.match(message, "%u%u%u%u%u%u%u%u%u%u") ~= nil then
		spam = true
		minetest.chat_send_player(name, "Please don't use too many caps!")
	end
	tell = false
	for blacklist_char, max in pairs(blacklist) do
		if countSubstring(message, blacklist_char) > max then
			tell = true
			spam = true
		end
	end
	if tell then
		minetest.chat_send_player(name, "Please don't spam!")
	end
	local player = minetest.get_player_by_name(name)
	local count = 0
	if player:get_attribute("spam") then
		count = tonumber(player:get_attribute("spam"))
	end
	if count >= 10 then
		player:set_attribute("spam", "0")
		local privs = minetest.get_player_privs(name)
		privs.shout = nil
		minetest.set_player_privs(name, privs)
		player:set_attribute("wait", "1")
		local spamlist = storage:get_string("spamlist")
		if spamlist ~= "" then
			local new_list = minetest.deserialize(spamlist)
			table.insert(new_list, name)
			storage:set_string("spamlist", minetest.serialize(new_list))
		else
			storage:set_string("spamlist", minetest.serialize({name}))
		end
		minetest.chat_send_player(name, "You have been warned! You have been reported to the admin right now")
	elseif spam then
		player:set_attribute("spam", tostring(count + 1))
		minetest.chat_send_player(name, "You have spammed " .. tostring(count + 1) .. " times, in 10 you'll be reported!")
	end
	if spam then
		return ""
	end
end)

minetest.register_on_joinplayer(function(player)
	local wait = player:get_attribute("wait")
	if wait then
		player:set_attribute("wait", nil)
		local privs = minetest.get_player_privs(player:get_player_name())
		privs.shout = true
		minetest.set_player_privs(player:get_player_name(), privs)
	end
	if player:get_player_name() == admin then
		if storage:get_string("spamlist") and storage:get_string("spamlist") ~= "{}" then
			minetest.chat_send_player(admin, "<antispam> Spammers list: " .. storage:get_string("spamlist"))
		else
			minetest.chat_send_player(admin, "<antispam> No spammers :)")
		end
	end
end)

minetest.register_chatcommand("spamlist", {params = "",
	description = "Shows the spamlist from antispam mod",
	privs = {["server"] = true,},
	func = function(name, param)
		minetest.chat_send_player(name, "<antispam> Spammers list: " .. storage:get_string("spamlist"))
		return true, ""
	end
})

minetest.register_chatcommand("clear_spamlist", {params = "",
	description = "Clears the spamlist from antispam mod",
	privs = {["server"] = true},
	func = function(name, param)
		storage:set_string("spamlist", "")
		return true, "List cleared"
	end
})
